var http = require('http');
var term = require('./termostato');

var servRes = '';

http.createServer(function(req, res){
	res.writeHead(200, {'Content-Type':'text/html'});

	if (req.method == 'GET') {
		res.write('Termostato : ' + term.getTerm());
	}

	if (req.method == 'PUT') {
		term.setOn();
	}

	if (req.method == 'DELETE'){
		term.setOff();
	}

	res.end();

}).listen(8080);  //si mette in ascolto all'indirizzo di default 127.0.0.1

console.log('HTTP server listening on http://127.0.0.1:8080/');