# Esercizio2

Implementare un server HTTP per gestire il riscaldamento di casa.
Implementare una classe termostato che fornisce i metodi per accendere e spegnere il termostato.
Quando il server riceve una chiamata http opportuna
(a voi definire quale, per esempio PUT per accendere e GET per spegner),
accende o spegne il termostato interagendo con la classe java termostato.