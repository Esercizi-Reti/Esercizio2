var mode = 'OFF';

module.exports.setOn = function(){
  mode = 'ON'
};

module.exports.setOff = function(){
  mode = 'OFF';
};

module.exports.getTerm = function(){
  return mode;
};